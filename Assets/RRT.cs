﻿using UnityEngine;
using System.Collections;

public class RRT : MonoBehaviour {
	private struct Point
	{
		public float x, y;
		public Point(float p1, float p2)
		{
			x = p1;
			y = p2;
		}
	}

	private struct Edge
	{
		public float x1, y1, x2, y2;

		public Edge(float x1, float y1, float x2, float y2)
		{
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
		}
	}


	class TreeNode
	{
		public float x, y;
		public TreeNode parent;

		public TreeNode(float x, float y, TreeNode parent)
		{
			this.x = x;
			this.y = y;
			this.parent = parent;
		}
	}

	static float[] polygonX;
	static float[] polygonY;
	static int[] endPoints;
	static int width, height;
	static Edge[] edges;
	static Point startPos, goalPos;
	static ArrayList treeList;
	static float distanceFromWalls;
	static bool skip;

	public static ArrayList RRTInit(int distance, int numNodes)
	{

		//Load the problem

//		polygonX = new float[] { 8.871f, 8.6406f, 21.083f, 32.373f, 58.871f, 77.535f, 53.571f, 13.94f, 14.862f, 56.106f, 72.926f, 94.585f, 93.433f, 69.009f, 75.461f, 75f, 37.673f, 35.599f, 19.47f, 28.917f, 29.378f, 17.166f, 12.097f };
//		polygonY = new float[] { 84.649f, 62.135f, 62.427f, 75.585f, 76.462f, 66.52f, 42.836f, 40.497f, 47.807f, 57.749f, 50.439f, 51.023f, 38.158f, 37.865f, 25.585f, 13.596f, 19.444f, 29.678f, 35.234f, 34.064f, 13.012f, 13.012f, 24.415f };
//		endPoints = new int[] { 1, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 1, 3, 1, 1, 1, 3, 1, 1, 1, 1, 3 };
//		width = 100;
//		height = 100;
//		startPos = new Point (80,30);
//		goalPos = new Point (30,70);

		polygonX = new float[] { 48.041f, 97.811f, 96.429f, 154.49f, 153.8f, 100.58f, 101.96f, 164.86f, 163.48f, 104.03f, 101.27f, 45.968f, 38.364f, 249.19f, 133.06f, 132.37f, 179.38f, 185.6f, 134.45f, 132.37f, 191.82f, 190.44f, 134.45f, 133.06f, 249.19f, 251.27f };
		polygonY = new float[] { 247.81f, 247.81f, 199.56f, 199.56f, 182.89f, 179.39f, 117.98f, 115.35f, 86.404f, 86.404f, 33.772f, 32.895f, 247.81f, 249.56f, 249.56f, 226.75f, 229.39f, 148.68f, 151.32f, 137.28f, 134.65f, 60.965f, 60.965f, 38.158f, 38.158f, 241.67f };
		endPoints = new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3 };
		width = 250;
		height = 250;
		startPos = new Point(120, 220);
		goalPos = new Point(150, 75);

		distanceFromWalls = 10;
		skip = true;

		//Get a list containing all the edges.
		buildEdgeArray();

		//Build the tree
		treeList = buildTree(startPos.x, startPos.y, distance, numNodes);

		//TODO We are not checking if the closest is an actual legal move. While it probably will not be a big problem, we should keep this in mind.
		//Find the vertex in the search tree closest to the end point
		TreeNode nearestToGoal = nearestNode(goalPos.x, goalPos.y, treeList);

		//Add the end point to the tree
		TreeNode currentNode = new TreeNode(goalPos.x, goalPos.y, nearestToGoal);
		treeList.Add(currentNode);


		// Return the path by going backwards and appending to the path starting at the goal
		ArrayList path = new ArrayList();
		path.Add(currentNode);
		while (currentNode.parent != null)
		{
			currentNode = currentNode.parent;
			path.Insert(0, currentNode);
		}

		//TODO Smoothing has been skipped, implement if time exist
		if(skip)
			SkipWaypoints(path);

		//Convert path to a number of points to move to
		ArrayList pointPath = new ArrayList();
		for (int i = 0; i < path.Count; i++)
		{
			TreeNode node = (TreeNode)path[i];
			scr.PointFloat point = new scr.PointFloat(node.x, node.y);
			pointPath.Add(point);
		}

		return pointPath;
	}


	//Create a datastructure with all the edges
	private static void buildEdgeArray()
	{
		ArrayList temp = new ArrayList();

		for(int i = 0; i<endPoints.Length; i++)
		{
			int start = i;

			while(endPoints[i] == 1)
			{
				temp.Add(new Edge(polygonX[i], polygonY[i], polygonX[i + 1], polygonY[i + 1]));
				i++;
			}
			temp.Add(new Edge(polygonX[i], polygonY[i], polygonX[start], polygonY[start]));
		}

		edges = new Edge[temp.Count];

		for (int j = 0; j < temp.Count; j++)
		{
			Edge e = (Edge)temp[j];
			edges[j] = e;
		}
	}


	//Buids a tree from a given root
	private static ArrayList buildTree(float x, float y, float distance, int numNodes)
	{

		System.Random random = new System.Random(); // Need randomization
		ArrayList treeList = new ArrayList();  //To store all the nodes in the tree

		TreeNode root = new TreeNode(x, y, null); //Root of the tree
		treeList.Add(root);


		float randomX, randomY;
		for (int i=0; i<numNodes; i++)
		{
			do
			{
				randomX = (float)random.NextDouble() * width;
				randomY = (float)random.NextDouble() * height;
			}
			//TODO Add safety distance
			//Randomize until you find an empty spot
			while (InsidePolygon(randomX, randomY) || distanceBuffer(randomX, randomY));

			//Find the node in tree that is the closest to the random point
			TreeNode nearest = nearestNode(randomX, randomY, treeList);

			//Make a node out of newly generated position
			TreeNode newNode = new TreeNode(randomX, randomY, nearest);

			//From the nearest node, move as close as allowed towards new node
			if (distance > 0)
				addTowardsNewNode(nearest, newNode, distance);

			//  try to add an edge if it can be linked without a collision occurring
			if (noCollision(newNode.x, newNode.y, nearest.x, nearest.y))
				treeList.Add(newNode);

		}
		return treeList;
	}

	//Figure out if a point is inside a polygon. Basically the solution that was posted on assignment site without any deeper understanding on our part
	private static bool InsidePolygon(float x, float y)
	{
		bool freeSpace = true;

		foreach (Edge e in edges)
			if (((e.y1 > y) != (e.y2 > y)) && (x < (e.x2 - e.x1) * (y - e.y1) / (e.y2 - e.y1) + e.x1))
				freeSpace = (!freeSpace);

		return !freeSpace; 
	}

	//Try to figure out the nearest node
	private static TreeNode nearestNode(float x, float y, ArrayList treeList)
	{
		TreeNode closestNode = null;
		float closestDistanceSquared = float.MaxValue;

		foreach (TreeNode node in treeList)
		{

			//TODO Dunno why source says squared. Better look into it. Otherwise code makes sense
			float distanceSquared = (x - node.x) * (x - node.x) + (y - node.y) * (y - node.y);

			if (distanceSquared < closestDistanceSquared)
			{
				closestDistanceSquared = distanceSquared;
				closestNode = node;
			}
		}

		return closestNode;
	}

	private static void addTowardsNewNode(TreeNode from, TreeNode to, float distance)
	{
		// calculate the angle
		float angle;
		if (from.x < to.x)
			angle = Mathf.Atan((to.y - from.y) / (to.x - from.x));
		else
			angle = 180 - Mathf.Atan((to.y - from.y) / (from.x - to.x));

		to.x = from.x + distance * Mathf.Cos(angle);
		to.y = from.y + distance * Mathf.Sin(angle);
	}

	//Go through each polygons edge and check for an intersection. If yes, then we are going through a wall which is a no go.
	private static bool noCollision(float x1, float y1, float x2, float y2)
	{
		foreach (Edge e in edges)
			if (lineIntersection(x1, y1, x2, y2, e.x1, e.y1, e.x2, e.y2))
				return false;

		return true;
	}

	//Checks if two line intersects.
	private static bool lineIntersection(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
	{
		float s1_x, s1_y, s2_x, s2_y;
		s1_x = x2 - x1; s1_y = y2 - y1;
		s2_x = x4 - x3; s2_y = y4 - y3;

		float s, t;
		s = (-s1_y * (x1 - x3) + s1_x * (y1 - y3)) / (-s2_x * s1_y + s1_x * s2_y);
		t = (s2_x * (y1 - y3) - s2_y * (x1 - x3)) / (-s2_x * s1_y + s1_x * s2_y);

		if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
		{
			// collision detected
			return true;
		}

		// no collision
		return false;
	}

	//If we can skip a waypoint by going from same first to third, skipping second we can remove that unecessary waypoint. Do this as long as it is possible.
	//This reduces number of waypoints and makes the path faster and simpler.
	private static void SkipWaypoints(ArrayList path)
	{
		bool changed = true;

		//As long as changes occurs, check for more possibilities
		while(changed)
		{
			changed = false;

			for(int i=0; i<path.Count-2; i++)
			{
				TreeNode current = (TreeNode)path[i];
				TreeNode next = (TreeNode)path[i + 2];

				if(noCollision(current.x,current.y,next.x,next.y))
				{
					path.RemoveAt(i + 1);
					//No need to fix parents, since that variable will not be used anymore at this point
					changed = true;
				}
			}
		}
	}


	private static bool distanceBuffer(float x, float y)
	{
		if (distanceFromWalls == 0)
			return false;

		foreach(Edge e in edges)
		{
			if (distancePointToLine(new Vector2(e.x1, e.y1), new Vector2(e.x2, e.y2), new Vector2(x, y)) < distanceFromWalls)
				return true;
		}

		return false;
	}


	//Translation of code from Stack Overflow http://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
	//I take no credit for coming up with this solution
	//Vertice v/w for the edge, p for the point
	private static float distancePointToLine(Vector2 v, Vector2 w, Vector2 p)
	{
		float l2 = (v.x-w.x)*(v.x - w.x) + (v.y - w.y) *(v.y-w.y);

		//If both edge vertice are the same
		if (l2 == 0.0)
			return distance(p, v);

		float t = Vector2.Dot(p - v, w - v) / l2;

		if (t < 0) return distance(p, v);
		else if (t > 1) return distance(p, w);

		Vector2 projection = v + t * (w - v);
		return distance(p, projection);

	}

	private static float distance(Vector2 v, Vector2 w)
	{
		return Mathf.Sqrt((v.x - w.x) * (v.x - w.x) + (v.y - w.y) * (v.y - w.y));
	}


}