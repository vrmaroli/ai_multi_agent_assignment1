﻿using UnityEngine;
using System;
using System.Collections;

public class scr : MonoBehaviour {
	Vector2 u = new Vector2 (0,0); // initial velocity
	string movingBody = "orc";
	bool facingTarget = false;
	float currentFacing = 0.0f;
	float angle = 1000.0f;
	ArrayList plannedPath = new ArrayList ();
	ArrayList plannedPathFiltered = new ArrayList ();
	int currentDestination = new int ();
	bool pathFound = false;
	bool reachedFinalDestination = false;
	bool[][] A;
	int mapMaxX, mapMaxY;
	public struct Point {
		public int x,y;
		public Point(int p1, int p2) {
			x = p1;
			y = p2;
		}
	}

	public struct PointFloat
	{
		public float x, y;
		public PointFloat(float p1, float p2)
		{
			x = p1;
			y = p2;
		}
	}

	public struct Edge
	{
		public float x1, y1, x2, y2;

		public Edge(float x1, float y1, float x2, float y2)
		{
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
		}
	}
	Point goalPos, startPos;
	float[] polygonX;
	float[] polygonY;
	int[] endPoints;
	Edge[] edges;

	//Get the width/height of the grid
	int width; 
	int height;

	//G is for calculating movement cost, F is the total cost, that is, G+H (H is calculated real time)
	//Parent is to remember the previous node travelled to get to the current one
	//Open is for nodes currently under consideration, while closed are already examined nodes
	int[,] G;
	int[,] F;
	Point[,] parent;
	ArrayList open = new ArrayList ();
	ArrayList closed = new ArrayList ();

	//A* for 16 movement directions
    public ArrayList Neighbour16(bool[][] grid, Point start, Point stop)
    {
        int width = grid[0].Length;
        int height = grid.Length;

        G = new int[height, width];
        F = new int[height, width];
        parent = new Point[height, width];

        //Set the starting values based on the start point
        open.Add(start);
        G[start.y, start.x] = 0;
        F[start.y, start.x] = HeuristicCost(start, stop);
        parent[start.y, start.x] = start;

        //Find the currently cheapest node to move to
        while (open.Count > 0)
        {
            Point currentNode = (Point)open[0];
            for (int i = 1; i < open.Count; i++)
            {

                Point compareNode = (Point)open[i];
                if (F[currentNode.y, currentNode.x] >=
                    F[compareNode.y, compareNode.x])
                    currentNode = compareNode;
            }
            if (currentNode.x == stop.x && currentNode.y == stop.y)
            {
                return reconstructPath(stop);
            }

            //Since we are now examining the current node, it goes to the closed heap to prevent it from being examined again
            closed.Add(currentNode);
            open.Remove(currentNode);

            //All possible cells we can move to. Probably should be at the start of the class but meh
            Point[] possibleCells = {

            new Point(1, 0),
            new Point(0, 1),
            new Point(-1, 0),
            new Point(0, -1),

            new Point(-1,-1),
            new Point(1,-1),
            new Point(-1, 1),
            new Point(1, 1),

            new Point(-2, -1),
            new Point(-2, 1),
            new Point(-1, -2),
            new Point(1, -2),
            new Point(2, -1),
            new Point(2, 1),
            new Point(-1, 2),
            new Point(1, 2)};

            //Now go through all 16 surrounding nodes and add those 
            for (int i = 0; i < possibleCells.Length; i++)
            {

                int x = currentNode.x + possibleCells[i].x;
                int y = currentNode.y + possibleCells[i].y;

                int xDiff = possibleCells[i].x;
                int yDiff = possibleCells[i].y;

                //Check grid boundaries
                if (x >= 0 && x < width)
                {
                    if (y >= 0 && y < height)
                    {
                        //Check that a point is passable and not already examined
                        if (grid[y][x] == false && closed.IndexOf(new Point(x, y)) == -1)
                        {

                            //When going diagonal, make sure we are not going through walls
                            if ((Mathf.Abs(xDiff) + Mathf.Abs(yDiff) == 1) || //We are moving west/east/north/south
                                ((Mathf.Abs(xDiff) + Mathf.Abs(yDiff) == 2) && (!grid[currentNode.y][x] && !grid[y][currentNode.x])) || //Moving diagonally
                                ((Mathf.Abs(xDiff) + Mathf.Abs(yDiff) == 3) && checkKnightMovement(currentNode.x, currentNode.y, xDiff, yDiff, grid)) //Move like a knight
                                )
                            {

                                //If the point has not been considered yet, add it to the open list and calculate G,F and parent
                                if (open.IndexOf(new Point(x, y)) == -1)
                                {

                                    if (Mathf.Abs(xDiff) + Mathf.Abs(yDiff) == 3)
                                        G[y, x] = G[currentNode.y, currentNode.x] + 22;
                                    else if (Mathf.Abs(xDiff) + Mathf.Abs(yDiff) == 2)
                                        G[y, x] = G[currentNode.y, currentNode.x] + 14;
                                    else
                                        G[y, x] = G[currentNode.y, currentNode.x] + 10;

                                    F[y, x] = G[y, x] + HeuristicCost(new Point(x, y), stop);
                                    parent[y, x] = currentNode;
                                    open.Add(new Point(x, y));
                                }
                                //If the point has been previously considered, update G,F and parent only if the new path is faster
                                else
                                {
                                    if (Mathf.Abs(xDiff) + Mathf.Abs(yDiff) == 3)
                                    {
                                        if (G[currentNode.y, currentNode.x] + 22 < G[y, x])
                                        {
                                            G[y, x] = G[currentNode.y, currentNode.x] + 22;
                                            F[y, x] = G[y, x] + HeuristicCost(new Point(x, y), stop);
                                            parent[y, x] = currentNode;
                                        }
                                    }
                                    else if (Mathf.Abs(xDiff) + Mathf.Abs(y) == 2)
                                    {
                                        if (G[currentNode.y, currentNode.x] + 14 < G[y, x])
                                        {
                                            G[y, x] = G[currentNode.y, currentNode.x] + 14;
                                            F[y, x] = G[y, x] + HeuristicCost(new Point(x, y), stop);
                                            parent[y, x] = currentNode;
                                        }
                                    }
                                    else if (G[currentNode.y, currentNode.x] + 10 < G[y, x])
                                    {
                                        G[y, x] = G[currentNode.y, currentNode.x] + 10;
                                        F[y, x] = G[y, x] + HeuristicCost(new Point(x, y), stop);
                                        parent[y, x] = currentNode;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }


    //Check that the path for a knight is clear
    private bool checkKnightMovement(int x, int y, int xOffset, int yOffset, bool[][] grid)
    {

        if (Mathf.Abs(xOffset) == 2)
        {
            if (xOffset == 2)
            {
                if (grid[y][x + 1] || grid[y + yOffset][x + 1])
                    return false;
            }
            else
            {
                if (grid[y][x - 1] || grid[y + yOffset][x - 1])
                    return false;
            }
        }
        else if (Mathf.Abs(yOffset) == 2)
        {
            if (yOffset == 2)
            {
                if (grid[y + 1][x] || grid[y + 1][x + xOffset])
                    return false;
            }
            else
            {
                if (grid[y - 1][x] || grid[y - 1][x + xOffset])
                    return false;
            }
        }

        return true;
    }

	//A* for eight movement directions
	public ArrayList Neighbour8(bool[][] grid, Point start, Point stop)
	{
		int width = grid[0].Length;
		int height = grid.Length;

		G = new int[height, width];
		F = new int[height, width];
		parent = new Point[height, width];

		//Set the starting values based on the start point
		open.Add(start);
		G[start.y, start.x] = 0;
		F[start.y, start.x] = HeuristicCost(start, stop);
		parent[start.y, start.x] = start;

		//Find the currently cheapest node to move to
		while (open.Count > 0)
		{
			Point currentNode = (Point)open[0];
			for (int i = 1; i < open.Count; i++)
			{

				Point compareNode = (Point)open[i];
				if (F[currentNode.y, currentNode.x] >=
					F[compareNode.y, compareNode.x])
					currentNode = compareNode;
			}
			//			Debug.Log (currentNode.x + "," + currentNode.y);
			//If goal is reached, return path
			if (currentNode.x == stop.x && currentNode.y == stop.y)
			{
				//				Debug.Log("count1");
				return reconstructPath(stop);
			}

			//Since we are now examining the current node, it goes to the closed heap to prevent it from being examined again
			closed.Add(currentNode);
			open.Remove(currentNode);

			//Now go through all 8 surrounding nodes and add those 
			for (int x = currentNode.x - 1; x <= currentNode.x + 1; x++)
			{
				for (int y = currentNode.y - 1; y <= currentNode.y + 1; y++)
				{
					//Check grid boundaries
					if (x >= 0 && x < width)
					{
						if (y >= 0 && y < height)
						{
							//Check that a point is passable and not already examined
							if (grid[y][x] == false && closed.IndexOf(new Point(x, y)) == -1)
							{

								//When going diagonal, make sure we are not going through walls
								if (!(x != currentNode.x && y != currentNode.y) ||
									(!grid[currentNode.y][x] && !grid[y][currentNode.x]))
								{

									//If the point has not been considered yet, add it to the open list and calculate G,F and parent
									if (open.IndexOf(new Point(x, y)) == -1)
									{
										if (x != currentNode.x && y != currentNode.y)
											G[y, x] = G[currentNode.y, currentNode.x] + 14;
										else
											G[y, x] = G[currentNode.y, currentNode.x] + 10;

										F[y, x] = G[y, x] + HeuristicCost(new Point(x, y), stop);
										parent[y, x] = currentNode;
										open.Add(new Point(x, y));
									}
									//If the point has been previously considered, update G,F and parent only if the new path is faster
									else
									{
										if (x != currentNode.x && y != currentNode.y)
										{
											if (G[currentNode.y, currentNode.x] + 14 < G[y, x])
											{
												G[y, x] = G[currentNode.y, currentNode.x] + 14;
												F[y, x] = G[y, x] + HeuristicCost(new Point(x, y), stop);
												parent[y, x] = currentNode;
											}
										}
										else if (G[currentNode.y, currentNode.x] + 10 < G[y, x])
										{
											G[y, x] = G[currentNode.y, currentNode.x] + 10;
											F[y, x] = G[y, x] + HeuristicCost(new Point(x, y), stop);
											parent[y, x] = currentNode;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return null;
	}

	//A* for four movement directions
	public ArrayList Neighbour4(bool[][] grid, Point start, Point stop)
	{
		int width = grid[0].Length;
		int height = grid.Length;

		G = new int[height, width];
		F = new int[height, width];
		parent = new Point[height, width];

		//Set the starting values based on the start point
		open.Add(start);
		G[start.y, start.x] = 0;
		F[start.y, start.x] = HeuristicCost(start, stop);
		parent[start.y, start.x] = start;

		//Find the currently cheapest node to move to
		while (open.Count > 0)
		{
			Point currentNode = (Point)open[0];
			for (int i = 1; i < open.Count; i++)
			{

				Point compareNode = (Point)open[i];
				if (F[currentNode.y, currentNode.x] >=
					F[compareNode.y, compareNode.x])
					currentNode = compareNode;
			}

			//If goal is reached, return path
			if (currentNode.x == stop.x && currentNode.y == stop.y)
			{
				//				Debug.Log("count1");
				return reconstructPath(stop);
			}

			//Since we are now examining the current node, it goes to the closed heap to prevent it from being examined again
			closed.Add(currentNode);
			open.Remove(currentNode);

			//Now go through all 8 surrounding nodes and add those 
			for (int x = currentNode.x - 1; x <= currentNode.x + 1; x++)
			{
				for (int y = currentNode.y - 1; y <= currentNode.y + 1; y++)
				{
					//Make sure it is not a diagonal
					if (!(x != currentNode.x && y != currentNode.y))
					{
						//Check grid boundaries
						if (x >= 0 && x < width)
						{
							if (y >= 0 && y < height)
							{
								//Check that a point is passable and not already examined
								if (grid[y][x] == false && closed.IndexOf(new Point(x, y)) == -1)
								{
									//If the point has not been considered yet, add it to the open list and calculate G,F and parent
									if (open.IndexOf(new Point(x, y)) == -1)
									{
										G[y, x] = G[currentNode.y, currentNode.x] + 10;
										F[y, x] = G[y, x] + HeuristicCost(new Point(x, y), stop);
										parent[y, x] = currentNode;
										open.Add(new Point(x, y));
									}
									//If the point has been previously considered, update G,F and parent only if the new path is faster
									else if (G[currentNode.y, currentNode.x] + 10 < G[y, x])
									{
										G[y, x] = G[currentNode.y, currentNode.x] + 10;
										F[y, x] = G[y, x] + HeuristicCost(new Point(x, y), stop);
										parent[y, x] = currentNode;
									}
								}
							}
						}
					}
				}
			}
		}
		return null;
	}

	//Once a path is found, reconstruct the path backwards, from the stop to the start, then reverse the result and return it
	ArrayList reconstructPath(Point stop)
	{
		ArrayList path = new ArrayList();
		Point current = stop;

		while (!(parent[current.y, current.x].x == current.x && parent[current.y, current.x].y == current.y))
		{
			path.Add(current);
			current = parent[current.y, current.x];
		}

		path.Reverse();

		return path;
	}

	//Manhattan distance
	int HeuristicCost(Point a, Point b)
	{
		return 10 * (Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y));
	}

	void discreteMotionModel() {
		if (pathFound && !reachedFinalDestination) {
			GameObject orc = GameObject.Find (movingBody);
			orc.transform.position = new Vector2 ((float)(1 * ((Point)plannedPath [currentDestination]).x) + (float)0.5, (float)(-1 * ((Point)plannedPath [currentDestination]).y) - (float)1.0);
			if (currentDestination < plannedPath.Count - 1) {
				currentDestination++;
			} else {
				reachedFinalDestination = true;
				Debug.Log ("Running time: " + Time.realtimeSinceStartup);
			}
		}
	}

	void kinematicPointModel() {
		float speed = 1.0f;
		if (pathFound && !reachedFinalDestination) {
			GameObject orc = GameObject.Find (movingBody);
			Vector2 target = new Vector2 ((float)(1 * ((Point)plannedPathFiltered [currentDestination]).x) + 0.5f, (float)(-1 * ((Point)plannedPathFiltered [currentDestination]).y) - 0.5f);
			Vector2 toTarget = (new Vector2(target.x - orc.transform.position.x,target.y - orc.transform.position.y)).normalized * speed;
			if (Mathf.Sqrt (Mathf.Pow (orc.transform.position.x - target.x, 2) + Mathf.Pow (orc.transform.position.y - target.y, 2)) < 0.4) {
				if (currentDestination < plannedPathFiltered.Count - 1) {
					currentDestination++;
				} else {
					reachedFinalDestination = true;
					Debug.Log ("Running time: " + Time.realtimeSinceStartup);
				}
			} else {
				orc.transform.position = new Vector2 (orc.transform.position.x + toTarget.x * Time.deltaTime, orc.transform.position.y + toTarget.y * Time.deltaTime );
			}
		}
	}


	void kinematicPointModelForPolygonMap() {
		float speed = 10.0f;
		if (pathFound && !reachedFinalDestination) {
			GameObject orc = GameObject.Find (movingBody);
			Vector2 target = new Vector2(((PointFloat)plannedPathFiltered[currentDestination]).x,((PointFloat)plannedPathFiltered[currentDestination]).y);
			Vector2 toTarget = (new Vector2(target.x - orc.transform.position.x,target.y - orc.transform.position.y)).normalized * speed;
			if (Mathf.Sqrt (Mathf.Pow (orc.transform.position.x - target.x, 2) + Mathf.Pow (orc.transform.position.y - target.y, 2)) < 0.4) {
				if (currentDestination < plannedPathFiltered.Count - 1) {
					currentDestination++;
				} else {
					reachedFinalDestination = true;
					Debug.Log ("Running time: " + Time.realtimeSinceStartup);
				}
			} else {
				orc.transform.position = new Vector2 (orc.transform.position.x + toTarget.x * Time.deltaTime, orc.transform.position.y + toTarget.y * Time.deltaTime );
			}
		}
	}

	void dynamicPointMassModel() {
		float force = 1.0f;
		//float maxVelocity = 10.0f;
		float t = Time.deltaTime;
		Vector2 a = new Vector2 (0, 0);
		if (pathFound && !reachedFinalDestination) {
			GameObject orc = GameObject.Find (movingBody);
			Rigidbody2D orcRigidObj = orc.GetComponent<Rigidbody2D>();
			Vector2 target = new Vector2 ((float)(1 * ((Point)plannedPathFiltered [currentDestination]).x) + 0.5f, (float)(-1 * ((Point)plannedPathFiltered [currentDestination]).y) - 0.5f);
			Vector2 toTarget = (new Vector2(target.x - orc.transform.position.x,target.y - orc.transform.position.y)).normalized * force;
			if (Mathf.Sqrt (Mathf.Pow (orc.transform.position.x - target.x, 2) + Mathf.Pow (orc.transform.position.y - target.y, 2)) < 0.5f) {
				if (currentDestination < plannedPathFiltered.Count - 1) {
					Debug.Log ("Moving on .. ");
					a = -0.8f * u / t;
					orc.transform.position = new Vector2(orc.transform.position.x + u.x*t + a.x*t*t/2, orc.transform.position.y + u.y*t + a.y*t*t/2);
					u.x = u.x + a.x * t;
					u.y = u.y + a.y * t;
					currentDestination++;
					if (currentDestination < plannedPathFiltered.Count - 1) {
						target = new Vector2 ((float)(1 * ((Point)plannedPathFiltered [currentDestination]).x) + 0.5f, (float)(-1 * ((Point)plannedPathFiltered [currentDestination]).y) - 0.5f);
						toTarget = (new Vector2(target.x - orc.transform.position.x,target.y - orc.transform.position.y)).normalized * force;
						if (Mathf.Sqrt (Mathf.Pow (orc.transform.position.x - target.x, 2) + Mathf.Pow (orc.transform.position.y - target.y, 2)) >= 0.5f) {
							a.x = toTarget.x / orcRigidObj.mass;
							a.y = toTarget.y / orcRigidObj.mass;
							orc.transform.position = new Vector2(orc.transform.position.x + u.x*t + a.x*t*t/2, orc.transform.position.y + u.y*t + a.y*t*t/2);
							u.x = u.x + a.x * t;
							u.y = u.y + a.y * t;
						}
					}
				} else {
					reachedFinalDestination = true;
					a = -1.0f * u / t;
					orc.transform.position = new Vector2(orc.transform.position.x + u.x*t + a.x*t*t/2, orc.transform.position.y + u.y*t + a.y*t*t/2);
					u.x = u.x + a.x * t;
					u.y = u.y + a.y * t;
					Debug.Log ("Running time: " + Time.realtimeSinceStartup);
				}
			} else {
				a.x = toTarget.x / orcRigidObj.mass;
				a.y = toTarget.y / orcRigidObj.mass;
				orc.transform.position = new Vector2(orc.transform.position.x + u.x*t + a.x*t*t/2, orc.transform.position.y + u.y*t + a.y*t*t/2);
				u.x = u.x + a.x * t;
				u.y = u.y + a.y * t;
			}
		}
	}

	void dynamicPointMassModelForPolygonMap()
	{
		float force = 1.0f;
		float t = Time.deltaTime;
		float maxVelocity = 10.0f;
		Vector2 a = new Vector2(0, 0);
		if (pathFound && !reachedFinalDestination)
		{
			GameObject orc = GameObject.Find(movingBody);
			Rigidbody2D orcRigidObj = orc.GetComponent<Rigidbody2D>();
			Vector2 target = new Vector2(((PointFloat)plannedPathFiltered[currentDestination]).x,((PointFloat)plannedPathFiltered[currentDestination]).y);
			Vector2 toTarget = (new Vector2(target.x - orc.transform.position.x, target.y - orc.transform.position.y)).normalized * force;
			if (Mathf.Sqrt(Mathf.Pow(orc.transform.position.x - target.x, 2) + Mathf.Pow(orc.transform.position.y - target.y, 2)) < 1.0f)
			{
				if (currentDestination < plannedPathFiltered.Count - 1)
				{
					Debug.Log("Moving on .. ");
					a = -1.0f * u / t;
					Vector2 s = new Vector2 (u.x * t + a.x * t * t / 2, u.y * t + a.y * t * t / 2);
					orc.transform.position = new Vector2(orc.transform.position.x + s.x, orc.transform.position.y + s.y);
					u.x = u.x + a.x * t;
					u.y = u.y + a.y * t;
					currentDestination++;
					if (currentDestination < plannedPathFiltered.Count - 1)
					{
						target = new Vector2(((PointFloat)plannedPathFiltered[currentDestination]).x,((PointFloat)plannedPathFiltered[currentDestination]).y);
						toTarget = (new Vector2(target.x - orc.transform.position.x, target.y - orc.transform.position.y)).normalized * force;
						if (Mathf.Sqrt (Mathf.Pow (orc.transform.position.x - target.x, 2) + Mathf.Pow (orc.transform.position.y - target.y, 2)) >= 1.0f) {
							a.x = toTarget.x / orcRigidObj.mass;
							a.y = toTarget.y / orcRigidObj.mass;
							s = new Vector2 (u.x * t + a.x * t * t / 2, u.y * t + a.y * t * t / 2);
							if ((Mathf.Sqrt (s.x * s.x + s.y * s.y) / t) > maxVelocity) {
								s = s.normalized * maxVelocity * t;
								u.x = s.x * t;
								u.y = s.y * t;
							} else {
								u.x = u.x + a.x * t;
								u.y = u.y + a.y * t;
							}
							orc.transform.position = new Vector2 (orc.transform.position.x + s.x, orc.transform.position.y + s.y);
						}
					}
				}
				else {
					reachedFinalDestination = true;
					a = -1.0f * u / t;
					orc.transform.position = new Vector2(orc.transform.position.x + u.x * t + a.x * t * t / 2, orc.transform.position.y + u.y * t + a.y * t * t / 2);
					u.x = u.x + a.x * t;
					u.y = u.y + a.y * t;
					Debug.Log ("Running time: " + Time.realtimeSinceStartup);
				}
			}
			else {
				a.x = toTarget.x / orcRigidObj.mass;
				a.y = toTarget.y / orcRigidObj.mass;
				Vector2 s = new Vector2 (u.x * t + a.x * t * t / 2, u.y * t + a.y * t * t / 2);
				if ((Mathf.Sqrt (s.x * s.x + s.y * s.y) / t) > maxVelocity) {
					s = s.normalized * maxVelocity * t;
					u.x = s.x * t;
					u.y = s.y * t;
				} else {
					u.x = u.x + a.x * t;
					u.y = u.y + a.y * t;
				}
				orc.transform.position = new Vector2(orc.transform.position.x + s.x, orc.transform.position.y + s.y);
			}
		}
	}


	void differentialDriveModel() {
		float speed = 1.0f;
		float rotateSpeed = 1.0f;
		if (pathFound && !reachedFinalDestination) {
			GameObject orc = GameObject.Find (movingBody);
			Vector2 target = new Vector2 ((float)(1 * ((Point)plannedPathFiltered [currentDestination]).x) + 0.5f, (float)(-1 * ((Point)plannedPathFiltered [currentDestination]).y) - 0.5f);
			Vector2 toTarget = (new Vector2(target.x - orc.transform.position.x,target.y - orc.transform.position.y)).normalized * speed;
			if (facingTarget) {
				if (Mathf.Sqrt (Mathf.Pow (orc.transform.position.x - target.x, 2) + Mathf.Pow (orc.transform.position.y - target.y, 2)) < 0.4) {
					if (currentDestination < plannedPathFiltered.Count - 1) {
						currentDestination++;
						facingTarget = false;
					} else {
						reachedFinalDestination = true;
						Debug.Log ("Running time: " + Time.realtimeSinceStartup);
					}
				} else {
					orc.transform.position = new Vector2 (orc.transform.position.x + toTarget.x * Time.deltaTime, orc.transform.position.y + toTarget.y * Time.deltaTime );
				}
			} else {
				// check facing direction
				float dot = Vector2.Dot(new Vector2(1,0).normalized, toTarget.normalized);
				float angleInRadians = Mathf.Acos(dot);
				float angle = Mathf.Rad2Deg * angleInRadians; //to degrees
				//Debug.Log("Angle: " + angle + "\tCurrent facing: " + currentFacing + "\tEuler Angle: " + orc.transform.eulerAngles.z);
				if (Mathf.Abs(currentFacing - angle) < rotateSpeed) {
					facingTarget = true;
				} else {
					if (currentFacing < angle) {
						currentFacing = (currentFacing + rotateSpeed)%360;
						orc.transform.Rotate (new Vector3 (0, 0, -rotateSpeed));
					} else if (currentFacing > angle) {
						currentFacing = (currentFacing - rotateSpeed)%360;
						orc.transform.Rotate (new Vector3 (0, 0, rotateSpeed));
					} else {
						// hope its not equal
					}
				}
			}
		}
	}

	void differentialDriveModelForPolygonMap()
	{
		float speed = 10.0f;
		float rotateSpeed = 1.0f;
		if (pathFound && !reachedFinalDestination)
		{
			GameObject orc = GameObject.Find(movingBody);
			Vector2 target = new Vector2(((PointFloat)plannedPathFiltered[currentDestination]).x,((PointFloat)plannedPathFiltered[currentDestination]).y);
			Vector2 toTarget = (new Vector2(target.x - orc.transform.position.x, target.y - orc.transform.position.y)).normalized * speed;
			if (facingTarget)
			{
				if (Mathf.Sqrt(Mathf.Pow(orc.transform.position.x - target.x, 2) + Mathf.Pow(orc.transform.position.y - target.y, 2)) < 0.4)
				{
					facingTarget = false;
					currentDestination++;
					if (! (currentDestination < plannedPathFiltered.Count))
					{
						reachedFinalDestination = true;
						Debug.Log ("Running time: " + Time.realtimeSinceStartup);
					}
				}
				else {
					orc.transform.position = new Vector2(orc.transform.position.x + toTarget.x * Time.deltaTime, orc.transform.position.y + toTarget.y * Time.deltaTime);
				}
			}
			else {
				// check facing direction
				float dot = Vector2.Dot(new Vector2(1, 0).normalized, toTarget.normalized);
				float angleInRadians = Mathf.Acos(dot);
				float angle = Mathf.Rad2Deg * angleInRadians; //to degrees
				//Debug.Log("Angle: " + angle + "\tCurrent facing: " + currentFacing + "\tEuler Angle: " + orc.transform.eulerAngles.z);
				if (Mathf.Abs(currentFacing - angle) < rotateSpeed)
				{
					facingTarget = true;
				}
				else {
					if (currentFacing < angle)
					{
						currentFacing = (orc.transform.localEulerAngles.z + rotateSpeed) % 360;
						orc.transform.Rotate(new Vector3(0, 0, +rotateSpeed));
					}
					else if (currentFacing > angle)
					{
						currentFacing = (orc.transform.localEulerAngles.z - rotateSpeed) % 360;
						orc.transform.Rotate(new Vector3(0, 0, -rotateSpeed));
					}
					else {
						// hope its not equal
					}
				}
			}
		}
	}

	void kinematicCarModelForPolygonMap() {
		float speed = 5.0f;
//		float L = 10.0f;
//		float phi = 45.0f * Mathf.Deg2Rad; // 45 degrees
//		float rotateSpeed = speed / L * Mathf.Tan (phi);
		float rotateSpeed = 10.0f;
		if (pathFound && !reachedFinalDestination) {
			GameObject orc = GameObject.Find (movingBody);
			if (facingTarget) {
				Vector2 target = new Vector2(((PointFloat)plannedPathFiltered[currentDestination]).x,((PointFloat)plannedPathFiltered[currentDestination]).y);
				Vector2 toTarget = (new Vector2(target.x - orc.transform.position.x,target.y - orc.transform.position.y)).normalized * speed;
				if (Mathf.Sqrt (Mathf.Pow (orc.transform.position.x - target.x, 2) + Mathf.Pow (orc.transform.position.y - target.y, 2)) < 0.4) {
					Debug.Log ("Moving on from " + currentDestination);
					facingTarget = false;
					currentDestination++;
					if (!(currentDestination < plannedPathFiltered.Count)) {
						reachedFinalDestination = true;
						Debug.Log ("Running time: " + Time.realtimeSinceStartup);
					}
				} else {
					//Debug.Log ("Now here...");
					orc.transform.position = new Vector2 (orc.transform.position.x + toTarget.x * Time.deltaTime, orc.transform.position.y + toTarget.y * Time.deltaTime );
				}
			} else {
				//Debug.Log ("" + currentDestination + "And now here... " + currentFacing + " ReachedFinalDest: " + reachedFinalDestination);
				// check facing, set true
				// correct facing, move maybe in a slower speed

				Vector2 target = new Vector2(((PointFloat)plannedPathFiltered[currentDestination]).x,((PointFloat)plannedPathFiltered[currentDestination]).y);
				Vector2 toTarget = (new Vector2(target.x - orc.transform.position.x,target.y - orc.transform.position.y)).normalized * speed;

				//float dot = Vector2.Dot(new Vector2(1, 0).normalized, toTarget.normalized);
				//float angleInRadians = Mathf.Acos(dot);
				//float angle = Mathf.Rad2Deg * angleInRadians; //to degrees

				angle = Mathf.Rad2Deg * Mathf.Atan2 (toTarget.y, toTarget.x);
				while (angle < 0.0f) {
					angle += 360.0f;
				}
				//angle = angle % 360.0f;
				Debug.Log("Angle: " + angle + "\tCurrent facing: " + currentFacing + "\tEuler Angle: " + orc.transform.eulerAngles.z);
				if (Mathf.Abs(currentFacing - angle) < 5.0f) {
					facingTarget = true;
				}
				else {
					if (currentFacing < angle)
					{
						currentFacing = (orc.transform.localEulerAngles.z + rotateSpeed * Time.deltaTime) % 360;
						orc.transform.Rotate(new Vector3(0, 0, +rotateSpeed * Time.deltaTime));
					}
					else if (currentFacing > angle)
					{
						currentFacing = (orc.transform.localEulerAngles.z - rotateSpeed * Time.deltaTime) % 360;
						orc.transform.Rotate(new Vector3(0, 0, -rotateSpeed * Time.deltaTime));
					}
					else {
						// hope its not equal
					}
					//Debug.Log ("Angle: " + angle + "\tCurrent facing: " + currentFacing + "\tEuler Angle: " + orc.transform.eulerAngles.z);
					toTarget = new Vector2 (Mathf.Cos (Mathf.Deg2Rad * currentFacing), Mathf.Sin (Mathf.Deg2Rad * currentFacing)).normalized * speed / 10 * Time.deltaTime;
					orc.transform.position = new Vector2 (orc.transform.position.x + toTarget.x, orc.transform.position.y + toTarget.y );
				}
			}
		}
	}

	void dynamicCarModelForPolygonMap () {
		float force = 1.0f;
		float t = Time.deltaTime;
		float maxVelocity = 10.0f;
		Vector2 a = new Vector2(0, 0);
		float speed = 1.0f;
//		float L = 10.0f;
//		float phi = 45.0f * Mathf.Deg2Rad; // 45 degrees
//		float rotateSpeed = Mathf.Sqrt (u.x * u.x + u.y * u.y) / L * Mathf.Tan (phi);
		float rotateSpeed = 20.0f;
		if (pathFound && !reachedFinalDestination) {
			GameObject orc = GameObject.Find (movingBody);
			Rigidbody2D orcRigidObj = orc.GetComponent<Rigidbody2D>();
			if (facingTarget) {
				Vector2 target = new Vector2(((PointFloat)plannedPathFiltered[currentDestination]).x,((PointFloat)plannedPathFiltered[currentDestination]).y);
				Vector2 toTarget = (new Vector2(target.x - orc.transform.position.x, target.y - orc.transform.position.y)).normalized * force;
				if (Mathf.Sqrt (Mathf.Pow (orc.transform.position.x - target.x, 2) + Mathf.Pow (orc.transform.position.y - target.y, 2)) < 0.4) {
					Debug.Log ("Moving on from " + currentDestination);
					facingTarget = false;
					a = -0.9f * u / t;
					orc.transform.position = new Vector2(orc.transform.position.x + u.x*t + a.x*t*t/2, orc.transform.position.y + u.y*t + a.y*t*t/2);
					u.x = u.x + a.x * t;
					u.y = u.y + a.y * t;
					currentDestination++;
					if (! (currentDestination < plannedPathFiltered.Count ))
					{
						reachedFinalDestination = true;
						Debug.Log ("Running time: " + Time.realtimeSinceStartup);
					}
				} else {
					//Debug.Log ("Now here...");
					a.x = toTarget.x / orcRigidObj.mass;
					a.y = toTarget.y / orcRigidObj.mass;
					Vector2 s = new Vector2 (u.x * t + a.x * t * t / 2, u.y * t + a.y * t * t / 2);
					if ((Mathf.Sqrt (s.x * s.x + s.y * s.y) / t) > maxVelocity) {
						s = s.normalized * maxVelocity * t;
						u.x = s.x * t;
						u.y = s.y * t;
					} else {
						u.x = u.x + a.x * t;
						u.y = u.y + a.y * t;
					}
					orc.transform.position = new Vector2(orc.transform.position.x + s.x, orc.transform.position.y + s.y);
				}
			} else {
				//Debug.Log ("" + currentDestination + "And now here... " + currentFacing + " ReachedFinalDest: " + reachedFinalDestination);
				// check facing, set true
				// correct facing, move maybe in a slower speed

				Vector2 target = new Vector2(((PointFloat)plannedPathFiltered[currentDestination]).x,((PointFloat)plannedPathFiltered[currentDestination]).y);
				Vector2 toTarget = (new Vector2(target.x - orc.transform.position.x,target.y - orc.transform.position.y)).normalized * force;

				float dot = Vector2.Dot(new Vector2(1, 0).normalized, toTarget.normalized);
				float angleInRadians = Mathf.Acos(dot);
				float angle = Mathf.Rad2Deg * angleInRadians; //to degrees
				//Debug.Log("Angle: " + angle + "\tCurrent facing: " + currentFacing + "\tEuler Angle: " + orc.transform.eulerAngles.z);
				if (Mathf.Abs(currentFacing - angle) < 5.0f) {
					facingTarget = true;
				}
				else {
					if (currentFacing < angle)
					{
						currentFacing = (orc.transform.localEulerAngles.z + rotateSpeed * Time.deltaTime) % 360;
						orc.transform.Rotate(new Vector3(0, 0, +rotateSpeed * Time.deltaTime));
					}
					else if (currentFacing > angle)
					{
						currentFacing = (orc.transform.localEulerAngles.z - rotateSpeed * Time.deltaTime) % 360;
						orc.transform.Rotate(new Vector3(0, 0, -rotateSpeed * Time.deltaTime));
					}
					else {
						// hope its not equal
					}
					//Debug.Log ("Angle: " + angle + "\tCurrent facing: " + currentFacing + "\tEuler Angle: " + orc.transform.eulerAngles.z);
					toTarget = new Vector2 (Mathf.Cos (Mathf.Deg2Rad * currentFacing), Mathf.Sin (Mathf.Deg2Rad * currentFacing)).normalized * force / 2 * Time.deltaTime;
					a.x = toTarget.x / orcRigidObj.mass;
					a.y = toTarget.y / orcRigidObj.mass;
					Vector2 s = new Vector2 (u.x * t + a.x * t * t / 2, u.y * t + a.y * t * t / 2);
					if ((Mathf.Sqrt (s.x * s.x + s.y * s.y) / t) > maxVelocity) {
						s = s.normalized * maxVelocity * t;
						u.x = s.x * t;
						u.y = s.y * t;
					} else {
						u.x = u.x + a.x * t;
						u.y = u.y + a.y * t;
					}
					orc.transform.position = new Vector2(orc.transform.position.x + s.x, orc.transform.position.y + s.y);
				}
			}
		}
	}

	void makeDiscreteMap() {
		// initialization of map variables
		A = new bool[][]{
			new bool[]{false,	false,	true,	false,	true,	true,	false,	true,	false,	false,	false,	false,	false,	true,	false,	false,	false,	false,	false,	false},
			new bool[]{false,	false,	false,	true,	true,	true,	false,	false,	false,	true,	false,	true,	false,	false,	true,	false,	true,	true,	false,	false},
			new bool[]{false,	false,	false,	false,	false,	false,	false,	true,	false,	false,	true,	true,	false,	false,	true,	false,	false,	true,	true,	false},
			new bool[]{false,	true,	false,	false,	false,	false,	true,	false,	false,	false,	true,	false,	false,	false,	true,	true,	false,	true,	false,	false},
			new bool[]{true,	false,	false,	true,	true,	false,	false,	true,	true,	true,	false,	false,	false,	false,	false,	true,	true,	false,	false,	false},
			new bool[]{false,	false,	true,	false,	false,	false,	false,	true,	false,	false,	false,	false,	false,	false,	false,	true,	false,	true,	false,	false},
			new bool[]{true,	false,	false,	false,	true,	false,	false,	false,	false,	true,	false,	false,	false,	false,	false,	false,	false,	true,	true,	false},
			new bool[]{false,	false,	false,	false,	false,	false,	false,	false,	true,	false,	true,	true,	false,	false,	true,	false,	true,	false,	false,	false},
			new bool[]{true,	false,	false,	false,	false,	false,	true,	true,	false,	true,	false,	true,	false,	false,	false,	true,	false,	false,	false,	true},
			new bool[]{false,	true,	false,	false,	false,	false,	false,	false,	false,	true,	true,	false,	false,	false,	false,	false,	false,	false,	false,	false},
			new bool[]{false,	false,	false,	true,	true,	true,	false,	true,	true,	false,	false,	false,	true,	true,	false,	true,	false,	true,	false,	false},
			new bool[]{true,	false,	false,	true,	false,	false,	true,	false,	false,	false,	true,	false,	true,	false,	false,	true,	true,	false,	true,	false},
			new bool[]{false,	false,	true,	false,	false,	false,	false,	false,	true,	true,	false,	false,	false,	true,	false,	false,	true,	false,	false,	true},
			new bool[]{false,	true,	false,	false,	false,	true,	true,	false,	false,	false,	true,	false,	true,	false,	false,	false,	false,	false,	false,	false},
			new bool[]{true,	false,	false,	false,	false,	false,	false,	false,	false,	true,	true,	false,	true,	false,	true,	false,	false,	false,	false,	false},
			new bool[]{false,	false,	false,	false,	false,	false,	false,	false,	false,	false,	false,	false,	false,	true,	false,	true,	false,	false,	false,	false},
			new bool[]{false,	false,	false,	true,	false,	false,	false,	true,	false,	true,	false,	false,	true,	true,	false,	false,	true,	false,	false,	false},
			new bool[]{true,	false,	false,	false,	true,	true,	false,	false,	false,	true,	false,	false,	false,	false,	false,	false,	false,	false,	true,	true},
			new bool[]{false,	true,	false,	false,	true,	false,	false,	false,	false,	false,	false,	false,	false,	true,	true,	false,	false,	false,	false,	false},
			new bool[]{false,	false,	true,	true,	true,	false,	false,	false,	false,	false,	true,	false,	true,	true,	false,	false,	false,	false,	false,	false}
		};
		mapMaxX = A[0].Length;
		mapMaxY = A.Length;
		goalPos = new Point (19, 19);
		startPos = new Point (0, 0);

		// start generating map from varialbles
		GameObject terrainBase = GameObject.CreatePrimitive(PrimitiveType.Cube);
		//cube.AddComponent<Rigidbody>();
		terrainBase.transform.localScale = new Vector2 (mapMaxX , mapMaxY );
		terrainBase.transform.position = new Vector3(mapMaxX/2, -1*mapMaxY/2, 1);
		terrainBase.name = "terrainBase";
		for (int i = 0; i < mapMaxX; i++) {
			for (int j = 0; j < mapMaxY; j++) {
				if(A[i][j]) {
					GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
					//cube.AddComponent<Rigidbody>();
					cube.transform.position = new Vector2((float)(1*j) + (float)0.5, (float)(-1*i) - (float)0.5);
					//cube.transform.localScale = new Vector3 ((float)0.95, (float)0.95, (float)1);
					cube.name = "wallUnit" + j + "X" + i + "Y";
				}
			}
		}

		GameObject startMarker = GameObject.CreatePrimitive (PrimitiveType.Cube);
		startMarker.transform.position = new Vector2 ((float)(1*startPos.x) + (float)0.5, (float)(-1*startPos.y) - (float)0.5);
		startMarker.transform.localScale = new Vector3 ((float)0.25, (float)0.25, (float)1);
		startMarker.name = "startMarker";

		GameObject goalMarker = GameObject.CreatePrimitive (PrimitiveType.Cube);
		goalMarker.transform.position = new Vector2 ((float)(1*goalPos.x) + (float)0.5, (float)(-1*goalPos.y) - (float)0.5);
		goalMarker.transform.localScale = new Vector3 ((float)0.50, (float)0.50, (float)1);
		goalMarker.name = "goalMarker";

		GameObject camObj = GameObject.Find ("camera");
		camObj.transform.position = new Vector3 (mapMaxX / 2, -1 * mapMaxY / 2, -15);
		Camera.main.orthographicSize = 13.0f;

		GameObject orc = GameObject.Find (movingBody);
		Rigidbody2D orcRigidObj = orc.AddComponent<Rigidbody2D>();
		orcRigidObj.gravityScale = 0.0f;
		//orc.transform.position = new Vector3 ((float)(1*startPos.x) + (float)0.5, (float)(-1*startPos.y) - (float)1.0, (float)(-1));
		orc.transform.position = new Vector3 ((float)(1*startPos.x) + (float)0.5, (float)(-1*startPos.y) - (float)0.5, (float)(-1));

		// path planning
		plannedPath = Neighbour4(A, startPos, goalPos);
		// plannedPath = Neighbour8(A, startPos, goalPos);

		if (plannedPath != null) {
			//Debug.Log ("Path found");
			pathFound = true;
		} else {
			// Debug.Log ("No path found");
			pathFound = false;
		}
		plannedPathFiltered = plannedPath;
		int loopVar = 0;
		while(loopVar < plannedPathFiltered.Count - 2) {
			// plannedPath[loopVar], plannedPath[loopVar + 1], plannedPath[loopVar + 2]
			float slope1 = (((Point)plannedPathFiltered[loopVar + 1]).y - ((Point)plannedPathFiltered[loopVar]).y)/(float)(((Point)plannedPathFiltered[loopVar + 1]).x - ((Point)plannedPathFiltered[loopVar]).x);
			float slope2 = (((Point)plannedPathFiltered[loopVar + 2]).y - ((Point)plannedPathFiltered[loopVar + 1]).y)/(float)(((Point)plannedPathFiltered[loopVar + 2]).x - ((Point)plannedPathFiltered[loopVar + 1]).x);
			if (slope1 == slope2) {
				//Debug.Log("slope comparison " + slope1 + ", " + slope2);
				plannedPathFiltered.RemoveAt (loopVar + 1);
			} else {
				loopVar++;
			}
		}
		currentDestination = 0;
		u = new Vector2 (0,0); // setting initial velocity
	}

	void buildEdgeArray() {
		ArrayList temp = new ArrayList();
		for (int i = 0; i < endPoints.Length; i++) {
			int start = i;
			while (endPoints [i] == 1) {
				temp.Add (new Edge (polygonX [i], polygonY [i], polygonX [i + 1], polygonY [i + 1]));
				i++;
			}
			temp.Add (new Edge (polygonX [i], polygonY [i], polygonX [start], polygonY [start]));
		}
		edges = new Edge[temp.Count];
		for (int j = 0; j < temp.Count; j++) {
			Edge e = (Edge)temp[j];
			edges[j] = e;
		}
	}

	void makeContinuousMap() {

//		polygonX = new float[] { 8.871f, 8.6406f, 21.083f, 32.373f, 58.871f, 77.535f, 53.571f, 13.94f, 14.862f, 56.106f, 72.926f, 94.585f, 93.433f, 69.009f, 75.461f, 75f, 37.673f, 35.599f, 19.47f, 28.917f, 29.378f, 17.166f, 12.097f };
//		polygonY = new float[] { 84.649f, 62.135f, 62.427f, 75.585f, 76.462f, 66.52f, 42.836f, 40.497f, 47.807f, 57.749f, 50.439f, 51.023f, 38.158f, 37.865f, 25.585f, 13.596f, 19.444f, 29.678f, 35.234f, 34.064f, 13.012f, 13.012f, 24.415f };
//		endPoints = new int[] { 1, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 1, 3, 1, 1, 1, 3, 1, 1, 1, 1, 3 };
//		width = 100;
//		height = 100;
//		startPos = new Point (80,30);
//		goalPos = new Point (30,70);

		polygonX = new float[] { 48.041f, 97.811f, 96.429f, 154.49f, 153.8f, 100.58f, 101.96f, 164.86f, 163.48f, 104.03f, 101.27f, 45.968f, 38.364f, 249.19f, 133.06f, 132.37f, 179.38f, 185.6f, 134.45f, 132.37f, 191.82f, 190.44f, 134.45f, 133.06f, 249.19f, 251.27f };
		polygonY = new float[] { 247.81f, 247.81f, 199.56f, 199.56f, 182.89f, 179.39f, 117.98f, 115.35f, 86.404f, 86.404f, 33.772f, 32.895f, 247.81f, 249.56f, 249.56f, 226.75f, 229.39f, 148.68f, 151.32f, 137.28f, 134.65f, 60.965f, 60.965f, 38.158f, 38.158f, 241.67f };
		endPoints = new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3 };
		width = 250;
		height = 250;
		startPos = new Point(120, 220);
		goalPos = new Point(150, 75);
		buildEdgeArray ();
		//ArrayList vertices = new ArrayList();
		for (int i = 0; i < edges.Length; i++) {
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cube.transform.position = new Vector2(edges[i].x1, edges[i].y1);
			cube.name = "vertex" + "_" + i;
			cube.transform.localScale = new Vector3 (0.25f, 0.25f, 0.25f);
			Vector2 direction = new Vector2 (edges[i].x2 - edges[i].x1, edges[i].y2 - edges[i].y1);
			for (float j = 0.0f; j < 1.0f; j+=0.001f) {
				GameObject babyCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
				babyCube.transform.position = new Vector2(edges[i].x1, edges[i].y1) + j * direction;
				babyCube.transform.localScale = new Vector3 (0.25f, 0.25f, 0.25f);
				babyCube.name = "wallUnit" + i + "_" + j;
			}
		}
		GameObject camObj = GameObject.Find ("camera");
		camObj.transform.position = new Vector3 (width / 2, height / 2, -15);
		Camera.main.orthographicSize = Mathf.Max (width, height) / 2;

		GameObject startMarker = GameObject.CreatePrimitive (PrimitiveType.Cube);
		startMarker.transform.position = new Vector2 ((float)startPos.x, (float)startPos.y);
		//startMarker.transform.localScale = new Vector3 ((float)0.25, (float)0.25, (float)1);
		startMarker.name = "startMarker";

		GameObject goalMarker = GameObject.CreatePrimitive (PrimitiveType.Cube);
		goalMarker.transform.position = new Vector2 ((float)goalPos.x, (float)goalPos.y);
		//goalMarker.transform.localScale = new Vector3 ((float)0.50, (float)0.50, (float)1);
		goalMarker.name = "goalMarker";

		GameObject orc = GameObject.Find (movingBody);
		Rigidbody2D orcRigidObj = orc.AddComponent<Rigidbody2D>();
		orcRigidObj.gravityScale = 0.0f;
		//orc.transform.position = new Vector3 ((float)(1*startPos.x) + (float)0.5, (float)(-1*startPos.y) - (float)1.0, (float)(-1));
		orc.transform.position = new Vector3 ((float)(1*startPos.x), (float)(1*startPos.y), (float)(-1));

		while (plannedPath != null && plannedPath.Count == 0) {
			plannedPath = RRT.RRTInit (20, 10000);
		}
		if (plannedPath != null) {
			Debug.Log ("Path found");
			pathFound = true;
			int j = 0;
			foreach (PointFloat i in plannedPath) {
				Debug.Log (j + " (" + i.x + "," + i.y + ")");
				j++;
			}
		} else {
			Debug.Log ("No path found");
			pathFound = false;
		}
		plannedPathFiltered = plannedPath;
		u = new Vector2 (0,0);
		currentDestination = 1;
	}

	// Use this for initialization
	void Start () {
		facingTarget = false;
		movingBody = "orc2";

		//makeDiscreteMap ();

		GameObject orc = GameObject.Find (movingBody);
		orc.transform.localScale =  new Vector3 (3.0f, 1.0f, 1.0f);
		makeContinuousMap ();
	}

	// Update is called once per frame
	void Update () {
	}

	void FixedUpdate() {
		// discreteMotionModel ();
		//dynamicPointMassModel ();
		// differentialDriveModel ();
		// kinematicPointModel ();

		//kinematicPointModelForPolygonMap ();
		// differentialDriveModelForPolygonMap ();
		// dynamicPointMassModelForPolygonMap ();
		kinematicCarModelForPolygonMap ();
		// dynamicCarModelForPolygonMap ();
	}
}